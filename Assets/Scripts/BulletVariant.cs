﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletVariant : MonoBehaviour
{
    public Vector2 direction;
    public float velocity;

    public bool isMultiple = false;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * velocity * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Finish" && isMultiple == false)
        {
            Destroy(transform.parent.gameObject);
        }
        else if (isMultiple == true && other.gameObject.tag == "Finish")
        {
            if (transform.parent.childCount > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }
}