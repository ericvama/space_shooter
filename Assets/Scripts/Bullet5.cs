﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet5 : MonoBehaviour
{
    public Vector2 direction;
    public float velocity;
    public float sentido;

    public float cambio;
    public float directionTime = 0;

    public bool isMultiple = false;

    // Update is called once per frame
    void Update()
    {
        directionTime += Time.deltaTime;
        transform.Translate(direction * velocity * Time.deltaTime);

        if (directionTime < cambio)
        {
            direction.x = 1;
            direction.y = 1 * sentido;
        }

        else if (directionTime < (cambio * 2))
        {
            direction.x = 1;
            direction.y = -1 * sentido;
        }

        else if (directionTime < (cambio * 3))
        {
            directionTime = 0;
        }

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Finish" && isMultiple == false)
        {
            Destroy(transform.parent.gameObject);
        }
        else if (isMultiple == true && other.gameObject.tag == "Finish")
        {
            if (transform.parent.childCount > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }
}