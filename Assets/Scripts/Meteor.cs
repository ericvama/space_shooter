﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 speed;
    public GameObject[] meteoritos;
    int seleccionado;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Awake()
    {
        for(int i=0; i < meteoritos.Length; i++)
        {
            meteoritos[i].SetActive(false);
        }

        seleccionado = Random.Range(0, meteoritos.Length); //el maximo no esta incluido, de 0 a 3
        meteoritos[seleccionado].SetActive(true);

        speed.x = Random.Range(-3, -1);
        speed.y = Random.Range(-4, 5);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate (speed * Time.deltaTime);
        meteoritos[seleccionado].transform.Rotate(0, 0, Random.Range(1, 100) * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Bullet")
        {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor()
    {
        //Desactivo el grafico
        meteoritos[seleccionado].SetActive(false);

        //Elimino el collider 2D
        Destroy(GetComponent<BoxCollider2D>());

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }


}
