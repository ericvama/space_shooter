﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay()
    {
        Debug.LogError("He pulsado play");

        SceneManager.LoadScene("Game");
    }

    public void PulsaExit()
    {
        Debug.LogError("He pulsado exit");

        Application.Quit();
    }

    public void PulsaCredits()
    {
        Debug.LogError("He pulsado credits");

        SceneManager.LoadScene("Creditos");
    }

    public void PulsaMenu()
    {
        Debug.LogError("He pulsado credits");

        SceneManager.LoadScene("MainMenu");
    }
}
