﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet4 : MonoBehaviour
{
    public Vector2 direction;
    public float velocity;

    public float cambio = 5;
    public float directionTime = 0;

    public bool isMultiple = false;

    // Update is called once per frame
    void Update()
    {
        directionTime += Time.deltaTime;
        transform.Translate(direction * velocity * Time.deltaTime);

        if(directionTime < cambio)
        {
            direction.x = 1;
            direction.y = 0;
        }

        else if (directionTime < (cambio * 2))
        {
            direction.x = 0;
            direction.y = 1;
        }

        else if (directionTime < (cambio*3))
        {
            direction.x = 1;
            direction.y = 0;
        }

        else if (directionTime < (cambio * 4))
        {
            direction.x = 0;
            direction.y = -1;
        }

        else if (directionTime < (cambio * 5))
        {
            direction.x = 1;
            direction.y = 0;
            directionTime = 0;
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Finish" && isMultiple == false)
        {
            Destroy(transform.parent.gameObject);
        }
        else if (isMultiple == true && other.gameObject.tag == "Finish")
        {
            if (transform.parent.childCount > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }
}