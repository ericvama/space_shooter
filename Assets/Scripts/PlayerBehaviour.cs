﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;

    public float shootTime = 0;

    public Weapon weapon;

    public Propeller prop;

    void Update () {
        shootTime += Time.deltaTime;

        transform.Translate(axis * speed * Time.deltaTime);

        if (transform.position.x > limits.x){
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);}
        else if (transform.position.x < -limits.x){
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);}

        if (transform.position.y > limits.y){
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);}
        if (transform.position.y < -limits.y){
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);}

        //limits.x = 4.5;
        //limits.y = 4.5;

        if (axis.x > 0)
        {
            prop.BlueFire();
        }
        else
        {
            prop.Stop();
        }


    }

    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;

    }

    public void Shoot()
    {
        if(shootTime > weapon.GetCadencia())
        {
            Debug.Log("Dispara");
            shootTime = 0;
            weapon.Shoot();
        }
        
    }
}
